---
title: "Victor Verbeke"

description: "Informaticien passionné."
cascade:
  featured_image: '/images/1606669359705.jpg'
---
Bienvenue !

Ici, je fais des petits posts quand l'envie me prend (pas très souvent) pour parler de divers sujets autour du code et de mes autres passions !

Vous pouvez trouver les articles ci-dessous. N'hésitez pas à me contacter si vous avez des questions, bonne lecture !
