---
date: 2023-05-20
featured_image: '/images/thumbnails/thumb1.jpg'
tags: ["OBS", "Code"]
title: "Comment coder un shader pour OBS"
show_reading_time: true

---
(Note à soi-même : écrire plus souvent des articles !)

Je stream sur Twitch, et j'ai tendance à vouloir des setups assez complexes. 
Je gère plusieurs pistes son pour calibrer parfaitement les différentes sources sonores selon les scènes, 
j'ai automatisé via du code et un bot différentes annonces dans le chat, et j'ai mis en place des interactions avec le public qui peuvent modifier l'apparence de mon stream.

Mais j'avoue, quand j'ai essayé d'avoir une scène qui tourne sur elle-même de manière perpétuelle, je n'ai pas trouvé. 
Alors, comme il ne me restait plus trop d'inconnues dans OBS afin de pouvoir prétendre maîtriser perfectement le logiciel, 
je me suis plongé dans ce que je connaissais le moins dans OBS : **les shaders**.

### Mais dis-moi Jamy, c'est quoi un shader ?
Un shader, c'est un bout de code ayant pour but de faire un rendu graphique avec éventuellement un flux vidéo (2D ou 3D) en entrée, pour obtenir un flux vidéo en sortie.
Grossièrement, un fond vert c'est un shader qui va prendre les pixels verts de votre image et les rendre transparents. 
En réalité, il est possible de faire plein de choses avec des shaders : détection de contour via des matrices de convolution, exclusion de couleurs, effets vidéo comme dans Snapchat, etc...

Je me suis donc demandé si c'était possible de faire un shader qui prend une image et qui la fait tourner continuellement autour d'un axe. Et bien oui, et je vais vous montrer avec code à l'appui.

### Conception du shader : comment OBS va gérer les fichiers .shader ?
OBS a besoin de définir des techniques pour comprendre ce que l'on veut faire avec les shaders. 
Il faut définir deux fonctions pour une technique : une pour la manipulation des vertices (que je n'aborderai pas : j'ai fait de la manipulation 2D donc aucun intérêt), et une pour la manipulation des pixels.
```
technique Draw
{
	pass
	{
		vertex_shader = mainTransform(v_in);
		pixel_shader  = mainImage(v_in);
	}
}
```

Une fois la technique définie, il faut bien entendu définir les fonctions de transformation, et donc on définit une fonction mainImage qui va prendre une matrice d'image en entrée pour rendre une matrice d'image en sortie.
```
float4 mainImage(VertData v_in) : TARGET
{
	float4 rgba = image.Sample(textureSampler, v_in.uv);
	float speed = (float)speed_percent * 0.1;
	float t = elapsed_time * speed;

	// use matrix to transform pixels
	float2 center_pixel_coordinates = float2(((float)center_width_percentage * 0.01), ((float)center_height_percentage * 0.01) );
	rgba = image.Sample(textureSampler, mul(v_in.uv - center_pixel_coordinates ,rotAxis(float3(0.0, 0.0, 1.0), (t))).xy + center_pixel_coordinates);

	return rgba;
}
```
Alors, qu'est-ce qu'il y a dans ce bout de code ? Entrons dans le détail :
* Tout d'abord, on définit un vecteur en quatre dimensions (rouge, vert, bleu, alpha, RGBA). v_in étant l'image d'entrée, v_in.uv correspond aux coordonnées `(x;y)` des pixels.
* On définit une vitesse de rotation à partir de speed_percent (qu'on verra plus tard, mais c'est une variable globale au shader)
* Le temps correspond à la vitesse fois le temps passé (plus la vitesse est grande, plus le "temps" va passer vite).
* On calcule les coordonnées du centre de l'image afin de le définir comme axe de rotation, dans un tableau 2D comme v_in.uv
* On prend les pixels de l'image et on les tourne autour de l'axe via la fonction rotAxis (qui prend en entrée un axe de rotation et le temps). 
	* Ici, l'axe Z est utilisé pour la rotation car l'image est définie sur le plan `(x;y)`.

La fonction rotAxis correspond donc à :
```
float3x3 rotAxis(float3 axis, float a) {
	float s=sin(a);
	float c=cos(a);
	float oc=1.0-c;

	float3 as=axis*s;

	float3x3 p=float3x3(axis.x*axis,axis.y*axis,axis.z*axis);
	float3x3 q=float3x3(c,-as.z,as.y,as.z,c,-as.x,-as.y,as.x,c);
	return p*oc+q;
}
```
Ici, on a des maths de transformations, mais il faut penser qu'un pixel en coordonnées (x;y) (relativement au centre de l'image) va garder sa distance, mais tourner de X degrés selon le temps.
On a donc à calculer l'angle de chaque pixel, la distance par rapport au centre, et on tourne l'image en déplaçant chaque pixel de l'angle donné.

On peut aussi vérifier comment on définit des paramètres pour les shaders sur OBS :
```
uniform float speed_percent = 1.0;
uniform bool Rotate_Transform = true;
uniform bool Rotate_Pixels = false;
uniform int center_width_percentage = 50;
uniform int center_height_percentage = 50;
```
Définies dans un scole global (pas dans des fonctions), ces variables deviennent interprétables et modifiables via les menus d'OBS.
Ce qui est intéressant, c'est `speed_percent` qui va définit la vitesse, et certains boutons pour définir si on veut activer/désactiver les effets du shader (que j'ai enlevé des extraits de code pour simplifier la lecture).
On peut aussi modifier la position du centre de rotation, ce qui peut donner des effets étranges !

En tout cas, c'était une expérience amusante ! Je ne vais pas non plus coder des shaders plus complexes, mais j'ai pu découvrir le monde des shaders.
Entre ceci et les shaders Minecraft que j'ai installé récemment, il y a un monde entier d'écart... mais ça me va !

#### Sources (c'est important !)

* Scripting Tutorial Halftone Filter : [https://obsproject.com/wiki/Scripting-Tutorial-Halftone-Filter](https://obsproject.com/wiki/Scripting-Tutorial-Halftone-Filter)
